#! /usr/bin/env python

import requests
import configargparse as cap
import socket
import logging, sys

class Config(object):

    def __init__(self):

        self.parser = cap.ArgParser(default_config_files=['/etc/ddnss.conf', '~/.config/ddnss.conf', './ddnss.conf'])
        self.parser.add_argument('-c', '--config', is_config_file=True, help='Configuration file to use')
        self.parser.add_argument('-k', '--api_key', required=True,type=str, action='append', help='API Key to use')
        self.parser.add_argument('-host', '--hosts', required=True, type=str, action='append', help='Hosts to update')
        self.parser.add_argument('--debug', action='store_true')
        tmp = self.parser.parse_known_args()
        self.__items = vars(tmp[0])

    def get_items(self):
        return self.__items.keys()

    def __getattr__(self, item):
        if item in self.__items.keys():
            return self.__items[item]
        else:
            raise ValueError(f"Whoops, could not find a config entry named {item}")


class Updater(object):

    current_ip = None
    host_ip = None
    host_name = None

    log = logging.getLogger("ddnss")

    def __init__(self, host_name=None):
        if host_name is not None:
            log.debug(f"Creating new Updater object for {host_name}")
            tmp_ip = requests.get('http://ipv4.icanhazip.com')
            self.current_ip = tmp_ip.content.decode("utf-8").rstrip()
            log.debug(f"Current WAN IP - {self.current_ip}")
            self.host_name = host_name
            self.host_ip = socket.gethostbyname(self.host_name)
            log.debug(f"Host name - {self.host_name} IP - {self.host_ip}")

    def requiresUpdate(self):
        ''' Does the host require an update? '''
        if self.host_ip == self.current_ip:

            return False
        else:
            return True

    def doUpdate(self, key):
        ''' Do the Update '''
        params = {'key': key, 'host': self.host_name}
        req = requests.get('https://ddnss.de/upd.php', params=params)
        if req.status_code == 200:
            return 'Okay'
        else:
            return f"Update failed with error code {req.status}"


logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s')
log = logging.getLogger('ddnss')
sh = logging.StreamHandler(sys.stdout)
sh.setLevel(logging.WARNING)
sh.setFormatter('%(asctime)s - %(levelname)s - %(message)s')
log.addHandler(sh)

if __name__ == '__main__':
    cfg = Config()
    if cfg.debug is True:
        sh.setLevel(logging.DEBUG)
    for host in cfg.hosts:
        updater = Updater(host)
        if updater.requiresUpdate() is True:
            log.info(f"{host} requires Update.")
            log.debug(f"Using API Key {cfg.api_key}")
            log.debug("Response: {updater.doUpdate(cfg.api_key)}")
        else:
            log.info(f"{host} is up to date.")